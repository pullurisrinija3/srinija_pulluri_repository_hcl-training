create database hcl_Srinija;

use hcl_Srinija;

CREATE TABLE admin(
email VARCHAR(100) PRIMARY KEY,
password VARCHAR(100)
);
insert into admin
values ("pullurisrinija3@gmail.com", "pullurisrinija3");
select * from admin;

CREATE TABLE user(
email VARCHAR(100) PRIMARY KEY,
password VARCHAR(100)
);
select * from user;


CREATE TABLE menu(
item_id int primary key,
item_name varchar(100),
item_price float
);

insert into menu value(1,"Spicy potato",450.50);
insert into menu value(2,"tortilla wrap",234.20);
insert into menu value(3,"Pasta",129.08);
insert into menu value(4,"pizza",456.98);
insert into menu value(5,"Italian nachos",246.00);
insert into menu value(6,"Raspberry dessert",129.00);
select * from menu;

CREATE TABLE my_cart(
email varchar(100),
item_id int,
item_name varchar(100),
number_of_plates int,
total_price float,
foreign key (email) references user(email),
foreign key (item_id) references menu(item_id),
primary key (email,item_id)
);

CREATE TABLE orders(
date_and_time varchar(50),
email varchar(100),
item_id int,
item_name varchar(100),
number_of_plate int,
total_price float,
foreign key (email) references user(email),
foreign key (item_id) references menu(item_id),
primary key (date_and_time,email,item_id)
);
