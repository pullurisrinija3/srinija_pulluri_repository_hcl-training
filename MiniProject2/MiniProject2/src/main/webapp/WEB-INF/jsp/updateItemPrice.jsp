<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Item Price</title>
<style type="text/css">
 body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }             
</style>
</head>
<body>
	<div align="center">
		
		<h1>SURABI RESTAURANT</h1>
		<hr >
	<div align="center">
		<a href="/admin/adminHome"><button class="btn1">Home</button></a>
		<a href="/admin/adminLogout"><button class="btn">Logout</button></a>
	</div>
	<hr>
	<a href="/menu/displayMenuForAdmin"><button class="btn1">Menu</button ></a>
	<hr>		 
		
	<h2>Menu Update</h2>
	<form action="/menu/updateItemPrice" method="post">
		<label>Id &nbsp &nbsp</label> <input type="text" name="id"
			required /><br>
		<br> <label>Price</label> <input type="text"
			name="price" required /><br>
		<br> <input type="submit" value="SUBMIT" class="btn1"> <br />
		<br />
	</form>
	</div>
	<hr>
	<div align="center">
	Status:
	<%
	Object result = request.getAttribute("objUpdateIPrice");
	if (result != null) {
		out.print(result + "<br>");
	}
	%></div>
</body>
</html>