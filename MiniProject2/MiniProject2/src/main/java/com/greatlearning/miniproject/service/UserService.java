package com.greatlearning.miniproject.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.dao.*;

@Service
public class UserService {

	@Autowired
	UserDao userDao;

	public String createAccount(User user) {
		if (userDao.existsById(user.getEmail())) {
			return "User details already present. Please Login";
		} else {
			userDao.save(user);
			return "User Registration Successful";
		}

	}
	
	public List<String> getAllUser() {
		List<User> listOfUser = userDao.findAll();
		List<String> listOfUserId = new ArrayList<String>();
		for (User user : listOfUser) {
			listOfUserId.add(user.getEmail());
		}
		return listOfUserId;
	}

	public String verifyPassword(User user) {
		if (userDao.existsById(user.getEmail())) {
			User u = userDao.getById(user.getEmail());

			if (u.getPassword().equals(user.getPassword())) {
				return "User Login Successful";
			} else {
				return "Password Wrong. Please Re-Enter";
			}
		} else {
			return "Details not Found. Register Now ";

		}
	}

	public String updatePassword(User user) {
		if (!userDao.existsById(user.getEmail())) {
			return "User Details not Found";
		} else {
			User u = userDao.getById(user.getEmail());
			u.setPassword(user.getPassword());
			userDao.saveAndFlush(u);
			return "Password Updated Successfully";
		}
	}

	public String deleteUser(String email) {
		if (!userDao.existsById(email)) {
			return "User details not Found. Re-Enter";
		} else {
			userDao.deleteById(email);
			return "User Deleted Successfully";
		}
	}
}
