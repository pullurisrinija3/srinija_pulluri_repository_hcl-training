package com.greatlearning.miniproject.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Order;

import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.DateAndTime;
import com.greatlearning.miniproject.bean.*;
import com.greatlearning.miniproject.dao.*;

@Service
public class OrdersService {
	@Autowired
	OrdersDao ordersDao;

	public List<Orders> getOldOrders(String email) {
		return ordersDao.getAllOrders(email);
	}
	
	public String storeOrder(List<MyCart> itemList, String email) {
		Orders orders = new Orders();
		Iterator<MyCart> ii = itemList.iterator();
		while (ii.hasNext()) {
			MyCart item = ii.next();
			orders.setKey(new CompositeKeyForOrder(DateAndTime.dateTime(), email, item.getKey().getItemId()));
			orders.setItemName(item.getItemName());
			orders.setNumberOfPlate(item.getNumberOfPlates());
			orders.setTotalPrice(item.getTotalPrice());

			ordersDao.save(orders);
		}
		return "";

	}

	public List<Orders> getNowOrders(String email) {
		List<Orders> listOfOrders = ordersDao.getAllOrders(email);
		System.out.println(listOfOrders);
		String today = DateAndTime.dateAndHour();
		System.out.println(today);
		List<Orders> myOrders = new ArrayList<Orders>();
		Iterator<Orders> ii = listOfOrders.iterator();
		while (ii.hasNext()) {
			Orders o = ii.next();
			if (o.getKey().getDateAndTime().contains(today)) {
				myOrders.add(o);
			}
		}
		System.out.println(myOrders);
		return myOrders;

	}

	public List<Orders> getTodaysOrders() {

		List<Orders> todaysOrders = new ArrayList<Orders>();
		List<Orders> listOfOrders = ordersDao.findAll();
		Iterator<Orders> ii = listOfOrders.iterator();
		while (ii.hasNext()) {
			Orders o = ii.next();
			if (o.getKey().getDateAndTime().contains(DateAndTime.today())) {
				todaysOrders.add(o);
			}
		}
		return todaysOrders;

	}

	public List<Orders> getMonthlyOrders() {

		List<Orders> monthOrders = new ArrayList<Orders>();
		List<Orders> listOfOrders = ordersDao.findAll();
		Iterator<Orders> ii = listOfOrders.iterator();
		while (ii.hasNext()) {
			Orders o = ii.next();
			if (o.getKey().getDateAndTime().contains(DateAndTime.thisMonth())) {
				monthOrders.add(o);
			}
		}
		return monthOrders;

	}

}
