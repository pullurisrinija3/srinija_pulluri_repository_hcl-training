package com.hcl.greatLearning.assignment3;

public class Book {
private int id;
private String name;
private double price;
private int noOfCopiesSold;
protected String genre;
protected String bookStatus;
public String getBookStatus() {
	return bookStatus;
}
public void setBookStatus(String bookStatus) {
	this.bookStatus = bookStatus;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public int getNoOfCopiesSold() {
	return noOfCopiesSold;
}
public void setNoOfCopiesSold(int noOfCopiesSold) {
	this.noOfCopiesSold = noOfCopiesSold;
}
public String getGenre() {
	return genre;
}
public void setGenre(String genre) {
	this.genre = genre;
}
public Book(int id,String name, double price, int noOfCopiesSold, String bookStatus,String genre) {
	super();
	this.id = id;
	this.name = name;    
	this.price = price;
	this.noOfCopiesSold = noOfCopiesSold;
	this.genre = genre;
	this.bookStatus=bookStatus;
}
public Book() {
	super();
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "Book [ id="+ id + ",name=" + name + ", price=" + price + ", noOfCopiesSold=" + noOfCopiesSold
			+", bookStatus="+bookStatus+ ", genre=" + genre + "]";
}

}
