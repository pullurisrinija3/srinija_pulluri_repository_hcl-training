package com.hcl.greatLearning.assignment3;
import java.util.Scanner;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
public class MagicOfBooks<T> extends Book
{
	Scanner sc=new Scanner(System.in);
	HashMap<Integer,Book> bm=new HashMap<>();
	TreeMap<Double,Book> tm=new TreeMap<>();
	ArrayList<Book> am=new ArrayList<>();
	public void addBook()
	{
		Book b=new Book();
		System.out.println("Enter the id of the book");
		b.setId(sc.nextInt());
		System.out.println("Enter the name of the book:");
		b.setName(sc.next());
		System.out.println("Enter the price of the book:");
		b.setPrice(sc.nextDouble());
		System.out.println("Enter the genre of the book:");
		b.setGenre(sc.next());
		System.out.println("Enter the noOfCopiesSold of the book:");
		b.setNoOfCopiesSold(sc.nextInt());
		System.out.println("Enter the status of the book:");
		b.setBookStatus(sc.next());
		bm.put(b.getId(), b);
		System.out.println("book added successfully");
		tm.put(b.getPrice(), b);
		am.add(b);
	}
	public void deleteBook()
	{
		System.out.println("Enter book id you want to delete:");
		int id=sc.nextInt();
		tm.remove(id);
	}
	public void updateBook()
	{
		Book b=new Book();
		System.out.println("Enter the id of the book");
		b.setId(sc.nextInt());
		System.out.println("Enter the name of the book:");
		b.setName(sc.next());
		System.out.println("Enter the price of the book:");
		b.setPrice(sc.nextDouble());
		System.out.println("Enter the genre of the book:");
		b.setGenre(sc.next());
		System.out.println("Enter the noOfCopiesSold of the book:");
		b.setNoOfCopiesSold(sc.nextInt());
		System.out.println("Enter the status of the book:");
		b.setBookStatus(sc.next());
		bm.replace(b.getId(), b);
		System.out.println("book details are updated successfully");
	}
	public void displayBookDetails()
	{
		if(bm.size()>0)
		{
			Set<Integer> keySet=bm.keySet();
			for(Integer key:keySet)
			{
				System.out.println(key+"-"+bm.get(key));
			}
		}
		else
		{
			System.out.println("no book is present in the store");
		}
	}
	public void count()
	{
		System.out.println("no.of books present in a store:"+bm.size());
		
	}
    public void autoBiography()
    {
    	String bestSelling="Autobiography";
    	ArrayList<Book> bookList=new ArrayList<Book>();
    	Iterator<Book> itr=(am.iterator());
    	while(itr.hasNext())
    	{
    		Book b1=(Book)itr.next();
    		if(b1.genre.equals(bestSelling))
    		{
    			bookList.add(b1);
    		}
    	}
    	System.out.println("The books which are under autobiography are:"+bookList);
    }
    public void displayByPrice(int flag)
    {
    	if(flag==1)
    	{
    		if(tm.size()>0)
    		{
    			Set<Double> keySet=tm.keySet();
   			for(Double key:keySet)
    			{
    				System.out.println(key+"-"+tm.get(key));
    				
    			}
    		}
    		else
    		{
    			System.out.println("no book is present in the store");
    		}
    	}
    	if(flag==2)
    	{
    		Map<Double,Object> treeMapReverseOrder=new TreeMap<>(tm);
    		NavigableMap<Double,Object> nmap=((TreeMap<Double,Object>)treeMapReverseOrder).descendingMap();
    		System.out.println("Book details:");
    		if(nmap.size()>0)
    		{
    			Set<Double> keySet=nmap.keySet();
    			for(Double key:keySet)
    			{
    				System.out.println(key+"-"+nmap.get(key));
    			}
    		}
    		else
    		{
    			System.out.println("no book is present in the store");
    		}
    	}
    	if(flag==3)
    	{
    		String bestSelling="BestSelling";
    		ArrayList<Book> bookList=new ArrayList<>();
    		Iterator<Book> itr=am.iterator();
    		while(itr.hasNext())
    		{
    			Book b=(Book)itr.next();
    			if(b.bookStatus.equals(bestSelling))
    			{
    			 bookList.add(b);	
    			}
    		}
    		System.out.println("the books which are bestselling in the store are:"+bookList);
    	}
    }
}
