package com.hcl.greatLearning.assignment3;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		 MagicOfBooks mb=new MagicOfBooks();
		 
			while (true) {
				System.out.println("1. add entry into book\r\n"
						+ "2. delete entries from book\r\n" + "3. update a book\r\n"
						+ "4. display all books \r\n" + "5. total count of books\r\n"
						+ "6. search for autobiography books" +  "\n7. display by features");
	       
				System.out.println("enter your choice:");
				int choice = sc.nextInt();

				switch (choice) {

				case 1://Add a book
					System.out.println("Enter a no. of books you want to add");
					int n=sc.nextInt();
					
					for(int i=1;i<=n;i++) {
						mb.addBook();
						}
					break;
				
				case 2://Delete a book
					   mb.deleteBook();
					    break;
		
				case 3://Update a book
					mb.updateBook();
					break;

				case 4:
					mb.displayBookDetails();
					break;
				
				case 5:
					System.out.println();
					mb.count();
					break;
					
				case 6:
					mb.autoBiography();
				      break;
				      
				
				case 7:
					System.out.println("Enter your choice:\n 1. Price low to high "
							+ "\n 2.Price high to low \n 3. Best selling");
					int ch = sc.nextInt();

					switch (ch) {

					case 1 : mb.displayByPrice(1);
					         break;
					case 2:mb.displayByPrice(2); 
					         break;
					case 3:mb.displayByPrice(3);
					        break;
					}
				case 8:
					System.out.println("Thank you!");
                default:
                	System.out.println("You have entered wrong choice.");
				}
			}
	}

}
