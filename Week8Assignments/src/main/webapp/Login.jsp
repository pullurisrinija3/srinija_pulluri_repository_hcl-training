<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to login page</title>
<style>
.button {
  background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  align="center";
}
</style>
<script> 
function validate()
{ 
     var userName=document.form.username.value; 
     var password=document.form.password.value;
 
     if (username==null||username=="")
     { 
     alert("username cannot be empty"); 
     return false; 
     }
     else if(password==null||password=="")
     { 
     alert("Password cannot be empty"); 
     return false; 
     } 
}
</script> 
</head>
<body>
<div style="text-align:center"><h1>Please login by entering following credentials</h1> </div>
    <br>
        <table border:1px; width:50px; height:50px" align="center"  cellpadding="3" cellspacing="3">
         <tr>
         <td style="font-size:20px;"><span style="font-weight: bold;">EmailID</span></td>
         <td><input type="text" name="EmailID"/></td>
         </tr>
         <tr>
         <td style="font-size:20px;"><span style="font-weight: bold;">username</span></td>
         <td><input type="text" name="username" /></td>
         </tr>
         <tr>
         <td style="font-size:20px;"><span style="font-weight: bold;">password</span></td>
         <td><input type="password" name="password" /></td>
         </tr>
         <tr>
         <td><span style="color:red"><%=(request.getAttribute("errorMessage") == null) ? ""
         : request.getAttribute("errorMessage")%></span></td>
         </tr>
         <tr>
         <td></td>
         </tr>
        </table>
        <p align="center">
        <input type="submit"  class="button" value="Login" name="Login" onclick="location.href='displayDashboard.spring';">
        <input type="reset" class="button" value="Reset">
         </p>
</body>
</html>