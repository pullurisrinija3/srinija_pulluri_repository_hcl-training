<%@page import="com.greatLearning.bean.Book"%>
<%@page import="com.greatLearning.dao.BookDao"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ListIterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
.button{
text-align="center";
font-size:20px;
font-weight:bold;
}
</style>
</head>
<body>
<h3 align="center" style="background-color:azimuth;">**********YOUR BOOKS TO READ LATER*******</h3>
<div>
<table align="center" cellpadding="7" cellspacing="7">
	<tr>
	        <th>count</th>
	        <th>BookImage</th>
			<th>BookId</th>
			<th>BookName</th>
			<th>BookType</th>
			<th>BookAuthor</th>
			<th>BookPrice</th>
	</tr>
<%
            if(session.getAttribute("addtoReadLaterBooks")!=null){
            	
            	ListIterator list=((List)session.getAttribute("addtoReadLaterBooks")).listIterator();
                BookDao bookdao=new BookDao();
	            int count=0,noOfBooks=0;
	            while(list.hasNext())
	            {
		          count++;
	              int BookId=Integer.parseInt((String)list.next());
		          Book book=bookdao.getBookByBookId(BookId);
		          noOfBooks++;
%>
		   <tr>
		        <td><%= count %></td>
		        <td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		        <td><%= book.getBookId() %></td>
		        <td><%= book.getBookName() %></td>
		        <td><%= book.getBookType() %></td>
		        <td><%= book.getBookAuthor() %></td>
		        <td><%= book.getBookPrice() %></td>
		       
		   </tr>
		   <%
		      }
	       %>
	       <tr>
	       <td colspan="7"><strong>noOfBooks = <%= noOfBooks%></strong></td>
	       <%
	          }
	       %>
	       </tr>
<tr>
<td style="text-align:center" colspan="6"><input type="button" value="logout" 
onclick="location.href='Logout.jsp'"></td>
</tr>
</table>
</div>
</body>
</html>