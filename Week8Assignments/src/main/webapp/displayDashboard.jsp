<%@page import="com.greatLearning.bean.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to dashboard</title>
<style>
*{
  box-sizing: border-box;
}
.column {
  float: left;
  width: 20%;
  padding: 3px;
}
.row::after {
  content: "";
  clear: both;
  display: table;
}
.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
function addtoLikedBooks(BookId)
    {
    alert("welcome to this section");
	    $.post("LikedBooks.jsp",{
        BookId:BookId
	    }, function(data,status)
	    {
		    
	    });
	    }
</script>
<script>
function addtoReadLaterBooks(BookId)
    {
	alert("Welcome to this section");
    	$.post("ReadLaterBooks.jsp",{
    	BookId:BookId
    	}, function(data,status)
    	{
    		
    	});
    	}
</script>
<h2 align="center">Welcome <%=request.getParameter("username") %>
<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	%>
</h2>
<p align="right">
<input class="button" type="button" value="logout" style="background-color:#008CBA;" onclick="location.href='BookStore.jsp';">
</p>
<a href="LikedBooks.jsp">Please click here to check your loved books</a><br/>
<a href="ReadLaterBooks.jsp">Please click here to check your books added to read later section</a><br/>
<form action="BookController" method="get">
<table align="center" cellpadding="7" cellspacing="7" >
<tr>
            <th style="font-size:20px">BookImage</th>
			<th style="font-size:20px">BookId</th>
			<th style="font-size:20px">BookName</th>
			<th style="font-size:20px">BookType</th>
			<th style="font-size:20px">BookAuthor</th>
			<th style="font-size:20px">BookPrice</th>
</tr>

<%
out.println(request.getAttribute("obj"));
Object obj = request.getAttribute("books");
List<Book> listOfBooks =(List<Book>)obj;
Iterator<Book> li = listOfBooks.iterator();
	while(li.hasNext()){
		Book book  = li.next();
		%>
		<tr>
		<td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		<td><p style="font-size:20px"><%=book.getBookId() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookName() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookType() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookAuthor() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookPrice() %></p></td>
		<td><input style="background-color:#008CBA;" class="button" type="button" value="Add to Liked Books" onclick="addtoLikedBooks('<%= book.getBookId()%>')"></td>
		<td><input style="background-color:#008CBA;" class="button" type="button" value="Add to Read later section" onclick="addtoReadLaterBooks('<%= book.getBookId()%>')"></td>
		</tr>
		<%
	}
%>
</form>
</body>
</html>