<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
*{
  box-sizing: border-box;
}
.column {
  float: left;
  width: 20%;
  padding: 3px;
}
.row::after {
  content: "";
  clear: both;
  display: table;
}
.button {
  background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
</head>
<body>
<h2 align="center" style="color:Purple; font-size: 35px; font-family:'Times New Roman';">welcome to the bookstore!!!!</h2>
<p align="right">
<input style="background-color:#008CBA;"  class="button" type="button" name="LOGIN" value="LOGIN" onclick="location.href='Login.jsp';">
<input style="background-color:#008CBA;" class="button" type="button" value="SIGNUP" onclick="location.href='Signup.jsp';">
</p>
<p align="right" style="color:Tomato;">*please signup if you are visiting the page for the first time.</p>
<div class="row">
  <div class="column">
    <img src="https://i.harperapps.com/hcanz/covers/9780008312879/x480.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:1</p>
    <p style="font-style: italic; font-size:20px;">BookName:The Everyday Hero Manifesto</p>
    <p style="font-style: italic; font-size:20px;">BookType:Economics</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Robin sharma</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 450.00</p>
  </div>
  <div class="column">
    <img src="https://th.bing.com/th/id/OIP.roIPRdZHY44oMB_pMBDx3AHaLJ?w=115&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:2</p>
    <p style="font-style: italic; font-size:20px;">BookName:Think like a monk</p>
    <p style="font-style: italic; font-size:20px;">BookType: Stress management</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Jay shetty</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 3000.03</p>
  </div>
  <div class="column">
    <img src="https://s3.ap-south-1.amazonaws.com/storage.commonfolks.in/docs/products/images_full/julius-caesar-shakespeare-s-greatest-stories-for-children_FrontImage_787.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:3</p>
    <p style="font-style: italic; font-size:20px;">BookName:Julius Caesar</p>
    <p style="font-style: italic; font-size:20px;">BookType: Literature</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:william shakespeare</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 650</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/51Ss0HI+PHL._SX490_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:4</p>
    <p style="font-style: italic; font-size:20px;">BookName:Disney mickey mouse club house</p>
    <p style="font-style: italic; font-size:20px;">BookType: comics</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:grace baranowski</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 650</p>
  </div>
  <div class="column">
    <img src="https://th.bing.com/th/id/OIP.jGfgAFGYB5IpyVAGtdga2QAAAA?w=115&h=180&c=7&r=0&o=5&dpr=1.5&pid=1.7" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:5</p>
    <p style="font-style: italic; font-size:20px;">BookName:hundred gifts</p>
    <p style="font-style: italic; font-size:20px;">BookType:fiction </p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:jennifer scott</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:750</p>
  </div>
</div>
<div class="row">
 <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/4193I4qLvfL._SX332_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:6</p>
    <p style="font-style: italic; font-size:20px;">BookName:Rush on the radio</p>
    <p style="font-style: italic; font-size:20px;">BookType:Arts & Photography</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:James golden</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:600.00</p>
  </div>
  <div class="column">
    <img src="https://m.media-amazon.com/images/I/81GY6msTP2L._AC_UY327_FMwebp_QL65_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:7</p>
    <p style="font-style: italic; font-size:20px;">BookName:hookers and blow</p>
    <p style="font-style: italic; font-size:20px;">BookType: comics</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Munty c.pepin</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 650.98</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/61qwFOWdNAS._SX400_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:8</p>
    <p style="font-style: italic; font-size:20px;">BookName:Avatar:the Last Air bender</p>
    <p style="font-style: italic; font-size:20px;">BookType:Arts & Photography</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Jenny dorsey</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:150</p>
  </div>
   <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/51JovABgzVL._SX339_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:9</p>
    <p style="font-style: italic; font-size:20px;">BookName:Marvel:By design</p>
    <p style="font-style: italic; font-size:20px;">BookType: Drawing</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Gestalen</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 750.00</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/411pJpTvdqS._SX405_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:10</p>
    <p style="font-style: italic; font-size:20px;">BookName:The Queen: 70 years of Majestic Style</p>
    <p style="font-style: italic; font-size:20px;">BookType:Fashion</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Bethan Holt</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:320.00</p>
  </div>
  </div>
  <div class="row">
   <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/51R7xSyotQL._SX370_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:11</p>
    <p style="font-style: italic; font-size:20px;">BookName:HR Giger.40th Ed.</p>
    <p style="font-style: italic; font-size:20px;">BookType: History</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Andreas J.Hirsch</p>
    <p style="font-style: italic; font-size:20px;">BookPrice: 430.50</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/41W2ODM020L._SY498_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:12</p>
    <p style="font-style: italic; font-size:20px;">BookName:BTS Calendar 2022</p>
    <p style="font-style: italic; font-size:20px;">BookType:Music</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:BTS Official</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:1150.99</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/511tEyCIqhL._SX377_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:13</p>
    <p style="font-style: italic; font-size:20px;">BookName:Art of Halo infinite</p>
    <p style="font-style: italic; font-size:20px;">BookType:Computer Technology</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Microsoft</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:756.00</p>
  </div>
   <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/51ryB-txJJL._SX397_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:14</p>
    <p style="font-style: italic; font-size:20px;">BookName:Windows 11 for dummies</p>
    <p style="font-style: italic; font-size:20px;">BookType:Computer Technology</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:Andy RathBone</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:789.00</p>
  </div>
  <div class="column">
    <img src="https://images-na.ssl-images-amazon.com/images/I/416Uc0RhQWL._SX327_BO1,204,203,200_.jpg" style="width:160px; height:160px;">
    <p style="font-style: italic; font-size:20px;">BookID:15</p>
    <p style="font-style: italic; font-size:20px;">BookName:The Judge's List: A Novel (The Whistler)</p>
    <p style="font-style: italic; font-size:20px;">BookType:Thrillers</p>
    <p style="font-style: italic; font-size:20px;">BookAuthor:John Grisham</p>
    <p style="font-style: italic; font-size:20px;">BookPrice:1023.90</p>
  </div>
</div>
</body>
</html>