package com.greatLearning.bean;

public class Signup {
	private String firstName;
	private String LastName;
	private String MiddleName;
	private String username;
	private String password;
	private String retypePassword;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		this.LastName = lastName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		this.MiddleName = middleName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRetypePassword() {
		return retypePassword;
	}
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	
	public Signup(String firstName, String lastName, String middleName, String username, String password,
			String retypePassword) {
		super();
		this.firstName = firstName;
		this.LastName = lastName;
		this.MiddleName = middleName;
		this.username = username;
		this.password = password;
		this.retypePassword = retypePassword;
		
	}
	public Signup() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Signup [firstName=" + firstName + ", LastName=" + LastName + ", MiddleName=" + MiddleName
				+ ", username=" + username + ", password=" + password + ", retypePassword=" + retypePassword
				+ "]";
	}
	

}
