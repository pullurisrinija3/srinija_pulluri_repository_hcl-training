package com.greatLearning.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatLearning.bean.Signup;
import com.greatLearning.dao.SignupDao;
@Service
public class SignupService {
	@Autowired
	SignupDao signupdao;
//	public String storeSignupDetails(Signup signup) {
//		if(signup.getFirstName()!=null||signup.getLastName()!=null||signup.getMiddleName()!=null
//		||signup.getUsername()!=null||signup.getPassword()!=null||signup.getRetypePassword()!=null) 
//		{
//		   return "data is stored in database";
//		}else {
//			return "Record didn't store";
//		}
//		}
	public String registerUser(String firstName,String LastName,String MiddleName,String username, String password,String retypePassword) {
		return signupdao.registerUser(firstName,LastName,MiddleName,username, password,retypePassword);
	}

}
