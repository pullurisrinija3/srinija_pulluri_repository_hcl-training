package com.greatLearning.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatLearning.bean.Book;
import com.greatLearning.dao.BookDao;

@Service
public class BookService {
	
@Autowired
BookDao bookdao;
public List<Book> getAllBooks() 
{
  return bookdao.retrieveBookDetails();
}
public Book getBookbyBookId(int BookId) {
	return bookdao.getBookByBookId(BookId);
}
public String addtoLikedBooks(int BookId, String BookName, String BookType,String BookAuthor, String BookImage,float BookPrice) {
	return bookdao.addtoLikedBooks(BookId, BookName,BookType,BookAuthor, BookImage,BookPrice);
}

}
