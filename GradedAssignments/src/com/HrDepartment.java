package com;

public class HrDepartment extends SuperDepartment 
{
	public String departmentName()
	{
		return "HR department";
	}
	public String getTodaysWork()
	{
		return "Fill todays worksheet and mark your attendance";
	}
	public String getWorkDeadLine()
	{
		return "Complete by EOD";
	}
	public String doActivity()
	{
		return "Team Lunch";
	}
	public void display()
	 {
		 System.out.println(departmentName());
		 System.out.println(getTodaysWork());
		 System.out.println(getWorkDeadLine());
		 System.out.println(doActivity());
		 System.out.println(isTodayAHoliday());
	 }
}
