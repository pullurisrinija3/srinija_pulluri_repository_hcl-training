package com;

public class TechDepartment extends SuperDepartment
{
	public String departmentName()
	{
		return "Tech department";
	}
	public String getTodaysWork()
	{
		return "complete coding of module 1";
	}
    public String getWorkDeadLine()
    {
    	return "complete by EOD";
    }
    public String getTechStackInformation()
    {
    	return "core java";
    }
    public void display()
	 {
		 System.out.println(departmentName());
		 System.out.println(getTodaysWork());
		 System.out.println(getWorkDeadLine());
		 System.out.println(getTechStackInformation());
		 System.out.println(isTodayAHoliday());
	 }
}
