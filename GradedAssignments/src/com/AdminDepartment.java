package com;

public class AdminDepartment extends SuperDepartment 
{
	 public String departmentName()
	 {
		 return "Admin Department";
	 }
	 public String getTodaysWork()
	 {
		 return "Complete your documents submission";
	 }
	 public String getWorkDeadLine()
	 {
		 return "Complete by EOD";
	 }
	 public void display()
	 {
		 System.out.println(departmentName());
		 System.out.println(getTodaysWork());
		 System.out.println(getWorkDeadLine());
		 System.out.println(isTodayAHoliday());
	 }

}
