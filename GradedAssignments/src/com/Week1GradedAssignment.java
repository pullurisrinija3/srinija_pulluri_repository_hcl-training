package com;

public class Week1GradedAssignment {

	public static void main(String[] args) {
		SuperDepartment sd = new SuperDepartment();
        sd.display();
		System.out.println();
		AdminDepartment ad = new AdminDepartment();
        ad.display();
        System.out.println();
        HrDepartment hr=new HrDepartment();
        hr.display();
        System.out.println();
        TechDepartment td=new TechDepartment();
        td.display();
	}

}
