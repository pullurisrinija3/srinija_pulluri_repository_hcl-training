package com.greatLearning.bean;

public class Login {
	private String EmailID;
	private String username;
	private String password;
	public String getEmailID() {
		return EmailID;
	}
	public void setEmailID(String EmailID) {
		this.EmailID = EmailID;
	}
	public String getUsername() {
	    return username;
	}
	public void setUserName(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Login(String EmailID, String username, String password) {
		super();
		this.EmailID = EmailID;
		this.username = username;
		this.password = password;
	}
	@Override
	public String toString() {
		return "login [EmailID=" + EmailID + ", username=" + username + ", password=" + password + "]";
	}
}
