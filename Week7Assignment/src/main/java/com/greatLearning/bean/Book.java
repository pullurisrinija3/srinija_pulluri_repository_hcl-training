package com.greatLearning.bean;

public class Book {
private int BookId;
private String BookName;
private String BookType;
private String BookAuthor;
private String BookImage;
private float BookPrice;
public int getBookId() {
	return BookId;
}
public void setBookId(int bookId) {
	this.BookId = bookId;
}
public String getBookName() {
	return BookName;
}
public void setBookName(String bookName) {
	this.BookName = bookName;
}
public String getBookType() {
	return BookType;
}
public void setBookType(String bookType) {
	this.BookType = bookType;
}
public String getBookAuthor() {
	return BookAuthor;
}
public void setBookAuthor(String bookAuthor) {
	this.BookAuthor = bookAuthor;
}
public String getBookImage() {
	return BookImage;
}
public void setBookImage(String bookImage) {
	this.BookImage = bookImage;
}
public float getBookPrice() {
	return BookPrice;
}
public void setBookPrice(float bookPrice) {
	this.BookPrice = bookPrice;
}
@Override
public String toString() 
{
	return "Book [BookId=" + BookId + ", BookName=" + BookName + ", BookType=" + BookType + ", BookAuthor=" + BookAuthor
			+ ", BookImage=" + BookImage + ", BookPrice=" + BookPrice + "]";
}
}
