package com.greatLearning.bean;

public class Signup {
	private String firstName;
	private String LastName;
	private String MiddleName;
	private String username;
	private String password;
	private String retypePassword;
	private String address;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String LastName) {
		this.LastName = LastName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		this.MiddleName = middleName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRetypePassword() {
		return retypePassword;
	}
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	
	public Signup() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Signup(String firstName, String lastName, String middleName, String username, String password,
			String retypePassword, String address) {
		super();
		this.firstName = firstName;
		LastName = lastName;
		MiddleName = middleName;
		this.username = username;
		this.password = password;
		this.retypePassword = retypePassword;
		this.address = address;
	}
	@Override
	public String toString() {
		return "Signup [username=" + username + ", firstName=" + firstName + ", LastName=" + LastName + ", MiddleName="
				+ MiddleName + ", address=" + address + ", password=" + password
				+ ", retypePassword=" + retypePassword + "]";
	}
	

}
