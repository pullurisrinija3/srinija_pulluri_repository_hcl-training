package com.greatLearning.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatLearning.bean.Book;
import com.greatLearning.bean.Login;
import com.greatLearning.dao.LoginDao;
import com.greatLearning.service.BookService;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		BookService bs=new BookService();
		List<Book> list=bs.getAllBookDetails();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",list);
		RequestDispatcher rd= request.getRequestDispatcher("BookStore.jsp");
		rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		String EmailID = request.getParameter("EmailID");
		String username = request.getParameter("username");
        String password = request.getParameter("password");
        Login login = new Login(EmailID,username,password);
        LoginDao loginDao = new LoginDao();
        String validateUser = loginDao.StoreLoginDetails(login);
        HttpSession hs=request.getSession();
	    hs.setAttribute("login",EmailID);
        response.getWriter().print(validateUser);
        	RequestDispatcher redi= request.getRequestDispatcher("displayDashboard.jsp");
        	redi.include(request, response);
        }
}

