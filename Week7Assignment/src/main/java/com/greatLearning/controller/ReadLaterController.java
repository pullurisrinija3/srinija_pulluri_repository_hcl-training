package com.greatLearning.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ReadLaterController
 */
@WebServlet("/ReadLaterController")
public class ReadLaterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadLaterController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		HttpSession session=request.getSession();
		List li;
		if(session.getAttribute("login")!=null) {
			String parameter=request.getParameter("BookId");
		
		if(session.getAttribute("addtoReadLaterBooks")==null) {
			li=new ArrayList();
			li.add(parameter);
			session.setAttribute("addtoReadLaterBooks", li);
			
		}else {
			li=(List)session.getAttribute("addtoReadLaterBooks");
			if(!li.contains(parameter)) {
			li.add(parameter);
			}
			session.setAttribute("addtoReadLaterBooks", li);
		}
		response.getWriter().print(li.size());
		}
		RequestDispatcher rd=request.getRequestDispatcher("ReadLaterBooks.jsp");
		rd.include(request,response);
	
	}

}
