package com.greatLearning.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatLearning.bean.Book;
import com.greatLearning.service.BookService;
/**
 * Servlet implementation class BookController
 */
@WebServlet("/BookController")
public class BookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		BookService bs=new BookService();
		List<Book> listOfBooks=bs.getAllBookDetails();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",listOfBooks);
		RequestDispatcher rd= request.getRequestDispatcher("BookStore.jsp");
		rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		PrintWriter pw=response.getWriter();
//		response.setContentType("text/html");
//		int BookId= Integer.parseInt(request.getParameter("BookId"));
//		String BookName=request.getParameter("BookName");
//		String BookType=request.getParameter("BookType");
//		String BookAuthor=request.getParameter("BookAuthor");
//		String BookImage=request.getParameter("BookImage");
//		float  BookPrice=Float.parseFloat(request.getParameter("BookPrice"));
//	    Book book=new Book();
//		book.setBookId(BookId);
//		book.setBookName(BookName);
//		book.setBookType(BookType);
//		book.setBookAuthor(BookAuthor);
//		book.setBookImage(BookImage);
//		book.setBookPrice(BookPrice);
//		BookService bs=new BookService();
//	    String res=bs.StoreBookDetails(book);
		   doGet(request,response);
//		pw.println(res);
//		RequestDispatcher rd=request.getRequestDispatcher("BookStore.jsp");
//		rd.include(request,response);

	}

}
