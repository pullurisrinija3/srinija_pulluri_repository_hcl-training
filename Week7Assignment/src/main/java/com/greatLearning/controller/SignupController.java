package com.greatLearning.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatLearning.bean.Signup;
import com.greatLearning.dao.SignupDao;

/**
 * Servlet implementation class SignupController
 */
@WebServlet("/SignupController")
public class SignupController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignupController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		String firstName = request.getParameter("firstName");
		String LastName = request.getParameter("LastName");
		String MiddleName = request.getParameter("MiddleName");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String retypePassword = request.getParameter("retypePassword");
		String address = request.getParameter("address");
		
		
		Signup signup = new Signup();
		   SignupDao signupDao = new SignupDao();
			
	        String validateUser = signupDao.StoreSignUpDetails(signup);
	        response.getWriter().print(validateUser);
	        RequestDispatcher redi= request.getRequestDispatcher("Login.jsp");
	        redi.include(request, response);
	        }
	        }
//	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		//super.doGet(req, resp);
//		HttpSession hs=request.getSession();
//		hs.setAttribute("obj",hs);
//		RequestDispatcher redi= request.getRequestDispatcher("Login2.jsp");
//    	redi.forward(request, response);
//	}



