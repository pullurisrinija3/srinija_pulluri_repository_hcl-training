package com.greatLearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import com.greatLearning.bean.Signup;

public class SignupDao {
	private String driver="com.mysql.cj.jdbc.Driver";
	public void ConnectiontoDB(String driver) {
		try {
			Class.forName(driver);
			
			
		}catch(Exception e) {
			System.out.println("exception caught"+e);
		}
	}public Connection getConnection() {
		Connection con=null;
		try {
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/week7Assignment_Srinija","root","Srinija123!");
		}catch(Exception e) {
			System.out.println(e);
		}
		return con;
	}
	public String StoreSignUpDetails(Signup signup)
	{
		ConnectiontoDB(driver);
		Connection con=getConnection();
		
		try
		{
			PreparedStatement ps=con.prepareStatement("insert into Signup values (?,?,?,?,?,?,?)"); 
            
            ps.setString(1,signup.getFirstName());
            ps.setString(2, signup.getLastName());
            ps.setString(3, signup.getMiddleName());
            ps.setString(4, signup.getUsername());
            ps.setString(5, signup.getPassword());
            ps.setString(6, signup.getRetypePassword());
            ps.setString(7, signup.getAddress());
            if(signup.getFirstName()==null || signup.getLastName()==null || signup.getMiddleName()==null||signup.getUsername()==null||signup.getPassword()==null
            ||signup.getRetypePassword()==null||signup.getAddress()==null) {
			System.out.println("Required fields can't be empty......!! \n TRY AGAIN");
			}else
			{
            ps.executeUpdate();
			}
		}
		catch(Exception e)
		{
			System.out.println("store method exception"+e);
			
			
		}
		return " ";
	}
}
