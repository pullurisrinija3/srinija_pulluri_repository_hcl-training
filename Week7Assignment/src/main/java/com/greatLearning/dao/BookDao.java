package com.greatLearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatLearning.bean.Book;
import com.greatLearning.resource.DatabaseResource;

public class BookDao {
//	public int StoreBookDetails(Book book){
//		try {
//			Connection con=DatabaseResource.getDbConnection();
//			PreparedStatement ps=con.prepareStatement("insert into Book values(?,?,?,?,?,?)");
//			ps.setInt(1,book.getBookId());
//			ps.setString(2,book.getBookName());
//			ps.setString(3,book.getBookType());
//			ps.setString(4, book.getBookAuthor());
//			ps.setString(5,book.getBookImage());
//			ps.setFloat(6,book.getBookPrice());
//			return ps.executeUpdate();
//		}
//		catch(Exception e)
//		{
//			System.out.println("store method exception"+e);
//			return 0;
//	    }
//	}
	public List<Book> retrieveBookDetails(){
		List<Book> listOfBooks=new ArrayList<>();
		try
		{
			Connection con=DatabaseResource.getDbConnection();
			PreparedStatement ps=con.prepareStatement("select * from Book");
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				Book b=new Book();
				b.setBookId(rs.getInt(1));
				b.setBookName(rs.getString(2));
				b.setBookType(rs.getString(3));
				b.setBookAuthor(rs.getString(4));
				b.setBookImage(rs.getString(5));
				b.setBookPrice(rs.getFloat(6));
				listOfBooks.add(b);
			}
		}catch(Exception e)
		{
			System.out.println("store method exception"+e);
			
		}
		return listOfBooks;

	}
		public Book getBookByBookId(int BookId) {
	    	List<Book> listOfBooks=new ArrayList<>();
	    	try {
	    		Connection con=DatabaseResource.getDbConnection();
				PreparedStatement pstmt = con.prepareStatement("select * from Book where BookId="+BookId);
				Book p = new Book();
				ResultSet rs = pstmt.executeQuery();
				 while(rs.next())
				 {						
					p.setBookId(rs.getInt(1));
					p.setBookName(rs.getString("BookName"));
					p.setBookType(rs.getString("BookType"));
					p.setBookAuthor(rs.getString("BookAuthor"));
					p.setBookImage(rs.getString("BookImage"));
					p.setBookPrice(rs.getFloat("BookPrice"));	
				 }
				  return p;
				}catch (Exception e) {
					System.out.println("In store method "+e);
				  }
			        return null;
	    	 }
		
		
	}
