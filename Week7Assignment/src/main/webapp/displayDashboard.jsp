<%@page import="com.greatLearning.bean.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to dashboard</title>
<style>
.button{
text-align="center";
font-size:15px;
font-weight:bold;
}
</style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
function addtoLikedBooks(BookId)
    {
    alert("welcome");
	    $.post("LikeController",{
        BookId:BookId
	    }, function(data,status)
	    {
		    alert(data);
	    });
	    }
</script>
<script>
function addtoReadLaterBooks(BookId)
    {
	alert("Welcome to this section");
    	$.post("ReadLaterController",{
    	BookId:BookId
    	}, function(data,status)
    	{
    		alert(data);
    	});
    	}
</script>
<h2 align="center">Welcome <%=request.getParameter("username") %></h2>
<p align="right">
<input type="button" value="logout" style="background-color:aqua;" onclick="location.href='BookStore.jsp';">
</p>
<form action="BookController" method="post">
<a href="LikedBooks.jsp">Please click here to check your loved books</a><br/>
<a href="ReadLaterBooks.jsp">Please click here to check your books added to read later section</a><br/>

<table align="center" cellpadding="7" cellspacing="7" >
<tr>
            <th>BookImage</th>
			<th>BookId</th>
			<th>BookName</th>
			<th>BookType</th>
			<th>BookAuthor</th>
			<th>BookPrice</th>
</tr>

<%
	Object obj = session.getAttribute("obj");
	List<Book> listOfBooks = (List<Book>)obj;
	Iterator<Book> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Book book  = ii.next();
		%>
		<tr>
		<td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		<td><p style="font-size:20px"><%=book.getBookId() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookName() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookType() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookAuthor() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookPrice() %></p></td>
		<td><input style="background-color:aqua;" type="button" value="Add to Liked Books" onclick="addtoLikedBooks('<%= book.getBookId()%>')"></td>
		<td><input style="background-color:aqua;" type="button" value="Add to Read later section" onclick="addtoReadLaterBooks('<%= book.getBookId()%>')"></td>
		</tr>
		<%
	}
%>
</table>

</form>
</body>
</html>