<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome to login page</title>
<style>
.button{
text-align="center";
font-size:20px;
font-weight:bold;
}
</style>
<script> 
function validate()
{ 
     var userName=document.form.username.value; 
     var password=document.form.password.value;
 
     if (username==null||username=="")
     { 
     alert("username cannot be empty"); 
     return false; 
     }
     else if(password==null||password=="")
     { 
     alert("Password cannot be empty"); 
     return false; 
     } 
}
</script> 
</head>
<body>

<div style="text-align:center"><h1>Please login by entering following credentials</h1> </div>
    <br>
    <form action="LoginController" method="post" onsubmit="javascript:return validate();">
        <table align="center">
         <tr>
         <td>EmailID</td>
         <td><input type="text" name="EmailID"/></td>
         </tr>
         <tr>
         <td>username</td>
         <td><input type="text" name="username" /></td>
         </tr>
         <tr>
         <td>password</td>
         <td><input type="password" name="password" /></td>
         </tr>
         <tr>
         <td><span style="color:red"><%=(request.getAttribute("errorMessage") == null) ? ""
         : request.getAttribute("errorMessage")%></span></td>
         </tr>
         <tr>
         <td></td>
         <td><input type="submit" value="Login" name="Login" onclick="location.href='displayDashboard.jsp';">
         <input type="reset" value="Reset"></td>
         </tr>
        </table>
    </form>
</body>
</html>