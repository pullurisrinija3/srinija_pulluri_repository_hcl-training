<%@page import="com.greatLearning.bean.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>welcome to book store</title>
<style>
.button{
text-align="center";
size:50;
font-size:20px;
font-weight:bold;
}
</style>
</head>
<body>
<h2 align="center" style=color:Purple>WELCOME TO THE BOOK STORE!!!!</h2>
<p align="right">
<input style="background-color:skyBlue;" type="button" name="login" value="login" onclick="location.href='Login.jsp';">
<input style="background-color:skyBlue;" type="button" value="signup" onclick="location.href='Signup.jsp';">
</p>
<p align="right" style="color:Tomato;">*please signup if you are visiting the page for the first time.</p>
<form action="BookController" method="post">

<table align="center" cellpadding="7" cellspacing="7">
	<tr>
	        <th>BookImage</th>
			<th>BookId</th>
			<th>BookName</th>
			<th>BookType</th>
			<th>BookAuthor</th>
			<th>BookPrice</th>
	</tr>
<%
	Object obj = session.getAttribute("obj");
	List<Book> listOfBooks = (List<Book>)obj;
	Iterator<Book> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Book book  = ii.next();
		%>
		<tr>
		<td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
	    <td><p style="font-size:20px"><%=book.getBookId() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookName() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookType() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookAuthor() %></p></td>
		<td><p style="font-size:20px"><%=book.getBookPrice() %></p></td>
		</tr>
		
		<%
		
	}
%>
</table>
</form>
</body>
</html>