<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.greatLearning.bean.Book"%>
<%@page import="java.util.List"%>
<%@page import="com.greatLearning.dao.BookDao"%>
<%@page import="com.greatLearning.resource.DatabaseResource"%>
<%@page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books added to like section</title>
</head>
<body>
<h3 align="center" style="background-color:azimuth;">**********YOUR LIKED BOOKS*******</h3>
<form action="LikeController" method="post"></form>
<div>
<table align="center" cellpadding="7" cellspacing="7">
	<tr>
	        <th>count</th>
	        <th>BookImage</th>
			<th>BookId</th>
			<th>BookName</th>
			<th>BookType</th>
			<th>BookAuthor</th>
			<th>BookPrice</th>
	</tr>
<%
            if(session.getAttribute("addtoLikedBooks")!=null){
            	
            	ListIterator list=((List)session.getAttribute("addtoLikedBooks")).listIterator();
                BookDao bookdao=new BookDao();
	            int count=0,noOfBooks=0;
	            while(list.hasNext())
	            {
		          count++;
	              int BookId=Integer.parseInt((String)list.next());
		          Book book=bookdao.getBookByBookId(BookId);
		          noOfBooks++;
%>
		   <tr>
		        <td><%= count %></td>
		        <td><img src="<%=book.getBookImage() %>" style="width:180px; height:160px;"></td>
		        <td><%= book.getBookId() %></td>
		        <td><%= book.getBookName() %></td>
		        <td><%= book.getBookType() %></td>
		        <td><%= book.getBookAuthor() %></td>
		        <td><%= book.getBookPrice() %></td>
		   </tr>
		   <%
		      }
	       %>
	       <tr>
	       <td colspan="7">noOfBooks = <%= noOfBooks%></td>
	       <%
	          }
	       %>
	       </tr>
<tr>
<td style="text-align:center" colspan="6"><input type="button" value="logout" 
onclick="location.href='Logout.jsp'"></td>
</tr>
</table>
</div>
</body>
</html>