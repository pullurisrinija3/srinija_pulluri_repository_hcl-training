package com;
import java.util.*;
public class DataStructureB
{
	public void cityNameCount(ArrayList<Employee> employees)
	{
		Set<String> set=new TreeSet<String>();
		ArrayList<String> array=new ArrayList<String>();
		for(Employee s:employees)
		{
			array.add(s.getCity());
			set.add(s.getCity());
		}
		for(String s:set)
		{
			System.out.print(s+"="+Collections.frequency(array,s)+",");
		}
	}
	public void monthlySalary(ArrayList<Employee> employees)
	{
		for(Employee e:employees)
		{
			System.out.print(e.getId()+"="+e.getSalary()/12+",");
		}
		System.out.println("\n");
		for(Employee e1:employees)
		{
		try
		{
            if (e1.getId()<0||e1.getAge()<0||e1.getSalary()<0||e1.getName()==null||e1.getDepartment()==null||e1.getCity()==null)
            {
               throw new IllegalArgumentException("Exception raised:value must not be null or empty");
            }
         }
         catch(IllegalArgumentException i) 
		{
        	 System.out.println(i);
	    }
	}
	}
}

