package com;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		ArrayList<Employee> employees= new ArrayList<>();  
		employees.add(new Employee(1,"Aman",20,"IT",1100000,"Delhi"));  
		employees.add(new Employee(2, "Bobby",22,"HR",500000,"Bombay"));  
		employees.add(new Employee(3, "Zoe", 20,"Admin",750000,"Delhi"));  
		employees.add(new Employee(4, "Smitha",21,"IT",1000000,"chennai"));  
		employees.add(new Employee(5, "Smitha",24,"HR",1200000,"Bengaluru"));  
		System.out.printf("%7s %14s %7s %10s %18s %13s", "S.no", "Name", "age", "Department", "salary","city");  
        System.out.println();
        for(Employee employee: employees)  
        {  
        System.out.format("%7s %14s %7s %10s %18s %13s", employee.getId(), employee.getName(), employee.getAge(),employee.getDepartment(),employee.getSalary(),employee.getCity());  
        System.out.println();   
        }
       DataStructureB obj=new DataStructureB();
       System.out.println("monthly salaries of all employees are:");
       obj.monthlySalary(employees);
       System.out.println("\n");
       Collections.sort(employees, new DataStructureA());
       System.out.println("Names of the employees in the Alphabetical order are:");
       System.out.println(employees);
       System.out.println();
       System.out.println("Count of all employees from same city are:");
       obj.cityNameCount(employees);      
}
}