package com;
import java.util.*;
public class Employee implements Comparable<Employee>
{
int id;
String name;
int age;
int salary; 
String department;
String city;
public int getId()
{
	return id;
}
public void setId(int id)
{
        this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
    	this.name = name;
    }
	
public int getAge() {
	return age;
}
public void setAge(int age) {
        this.age= age;
    }
public double getSalary() {
	return salary;
}
public void setSalary(int salary) {
        this.salary = salary;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	
	this.department = department;
    }
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
    }

@Override
public int compareTo(Employee o) {
    return this.id - o.id;
}
public Employee()
{  
super();  
}  
public Employee(int id, String name, int age,String department, int salary,String city)throws IllegalArgumentException
{  
super();
if(id<0)
{
	throw new IllegalArgumentException("exception occured,id cannot be less than zero");
}
this.id = id;
if(name==null)
{
	throw new IllegalArgumentException("exception occured,name cannot be empty");
}
this.name = name;    
if(age<0)
{
	throw new IllegalArgumentException("exception occured,age cannot be less than zero");
}
this.age = age;  
if(salary<0)
{
	throw new IllegalArgumentException("exception occured,salary cannot be less than zero");
}
this.salary = salary;  
if(department==null)
{
	throw new IllegalArgumentException("exception occured,department cannot be null");
}
this.department=department;
if(city==null)
{
	throw new IllegalArgumentException("exception occured,city cannot be null");
}
this.city=city;

}  
@Override
public String toString() {
    return  name;
}

}
