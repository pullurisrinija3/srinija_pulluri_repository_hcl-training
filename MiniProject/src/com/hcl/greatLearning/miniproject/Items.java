package com.hcl.greatLearning.miniproject;

public class Items {
	private int id;
	private String nameOfItem;
	private int itemQuantity;
	private double itemPrice;
	
	public Items(int id, String nameOfItem, int itemQuantity, double itemPrice) throws IllegalArgumentException{
		super();
		if(id<0)
		{
			throw new IllegalArgumentException("exception occured, id cannot less than zero");
		}
		this.id = id;
		
		if(nameOfItem==null) {
			throw new IllegalArgumentException("exception occured, name cannot be null");

		}
		this.nameOfItem = nameOfItem;
		if(itemQuantity<0)
		{
			throw new IllegalArgumentException("exception occured, quantity cannot less than zero");
		}
		
		this.itemQuantity = itemQuantity;
		
		if(itemPrice<0)
		{
			throw new IllegalArgumentException("exception occured, price cannot less than zero");
		}
		this.itemPrice = itemPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNameOfItem() {
		return nameOfItem;
	}

	public void setNameOfItem(String nameOfItem) {
		this.nameOfItem = nameOfItem;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "" + id + " " + nameOfItem + " " + itemQuantity + " "+ itemPrice  ;
	}
}
