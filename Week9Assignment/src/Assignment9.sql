create database week9Assignment_Srinija;

use week9Assignment_Srinija;

create table Book(id int primary key,BookName varchar(60),BookType varchar(60),BookAuthor varchar(60),BookPrice float);

insert into Book values(1,"The Everyday Hero Manifesto","Economics","Robin sharma",450.00);

insert into Book values(2,"Think like a monk","Stress management","Jay shetty",300.03);

insert into Book values(3,"julius caesar","Literature","William shakespeare",650);

insert into Book values(4,"Disney mickey mouse club house","comics","grace baranowski",65.00);

insert into Book values(5,"hookers and blow","comics","Munty C. Pepin",65.98);

insert into Book values(6,"The hundred gifts","fiction","Jennifer scott",750.00);

insert into Book values(7,"Rush on the radio","Arts & photography","James Golden",50.00);

insert into Book values(8,"Avatar:The Last Airbender",150.00);

insert into Book values(9,"Marvel By Design ","Drawing","gestalten",234.00);

insert into Book values(10,"The Queen: 70 years of Majestic Style","fashion","Bethan holt",520.00);

select * from Book;

create table Login(loginid int primary key,EmailID varchar(50),username varchar(30),password char(32));

select * from Login;

create table SignUp(signupid int primary key,firstName varchar(30),LastName varchar(50),MiddleName varchar(40),username varchar(40),password varchar(32));

select * from SignUp;

create table Users(userId int primary key,username varchar(60),Email varchar(50),password varchar(50),retypePassword varchar(60));

select * from Users;

create table likedBooks(id int primary key,username varchar(40),BookName varchar(60),BookType varchar(60),BookAuthor varchar(60),BookImage varchar(250),BookPrice float);

select * from likedBooks;

create table Admin(adminid int primary key,name varchar(60),Email varchar(50),password varchar(50));

select * from Admin;







