package com.greatLearning.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatLearning.bean.Book;
import com.greatLearning.dao.BookDao;
@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	public String storeBookDetails(Book book)
	{	
		if(bookDao.existsById(book.getId())) 
		{
		  return "BookId must be unique";
		}else {
		  bookDao.save(book);
		  return "Books stored successfully";
		}
    }
	
	public String deleteBooks(int BookId)
	{
		if(!bookDao.existsById(BookId)) {
			return "books are not present";
			}else {
			bookDao.deleteById(BookId);
			return "entered book deleted successfully";
			}	
	}
	
	public String updateBooks(Book book) {
		if(!bookDao.existsById(book.getId())) 
		{
		    return "Books are not present with the given id";
		}
		else 
		{
			Book b	= bookDao.getById(book.getId()); 
			b.setBookPrice(book.getBookPrice());					
			bookDao.saveAndFlush(b);				
			return "Book updated successfully";
		}	
	}
	
	public List<Book> getAllBooks(){
		return bookDao.findAll();
	}
}
