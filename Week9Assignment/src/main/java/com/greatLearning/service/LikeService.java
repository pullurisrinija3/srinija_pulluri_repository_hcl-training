package com.greatLearning.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatLearning.bean.Like;
import com.greatLearning.dao.LikeDao;
@Service
public class LikeService {
	@Autowired
	LikeDao likedao;
	
	public String storeLikedBookDetails(Like like)
	{	
		if(likedao.existsById(like.getId())) 
		{
		  return "BookId must be unique";
		}else {
		  likedao.save(like);
		  return "Books stored successfully";
		}
    }
	public List<Like> getAllLikedBooks(){
		return likedao.findAll();
	}
}
