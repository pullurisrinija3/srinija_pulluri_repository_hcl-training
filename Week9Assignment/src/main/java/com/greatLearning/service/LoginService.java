package com.greatLearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatLearning.bean.Login;
import com.greatLearning.dao.LoginDao;
@Service
public class LoginService {
	@Autowired
	LoginDao loginDao;
	public String storeLoginDetails(Login login)
	{	
		if(loginDao.existsById(login.getLoginid())) 	
		{
		  return "required fields are empty";
		}else {
		  loginDao.save(login);
		  return "login successfull";
		}
    }
	public List<Login> getAllLoginDetails(){
		return loginDao.findAll();
	}
	
}
