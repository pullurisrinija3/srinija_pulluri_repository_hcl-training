package com.greatLearning.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatLearning.bean.SignUp;
import com.greatLearning.dao.SignUpDao;
@Service
public class SignUpService {
	@Autowired
	SignUpDao signupDao;
	public String storeSignUpDetails(SignUp signup)
	{	
		if(signupDao.existsById(signup.getSignupid())) 
		{
		  return "required fields are empty";
		}else {
		  signupDao.save(signup);
		  return "signup successfull";
		}
    }
	public List<SignUp> getAllSignUpDetails(){
		return signupDao.findAll();
	}
}
