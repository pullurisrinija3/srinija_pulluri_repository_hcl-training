package com.greatLearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatLearning.bean.SignUp;
@Repository
public interface SignUpDao extends JpaRepository<SignUp,Integer>{

}
