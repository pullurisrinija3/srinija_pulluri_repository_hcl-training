package com.greatLearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.greatLearning.bean.Like;

@Repository
public interface LikeDao  extends JpaRepository<Like,Integer>{

}
