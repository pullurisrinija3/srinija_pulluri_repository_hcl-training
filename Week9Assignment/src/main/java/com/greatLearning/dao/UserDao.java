package com.greatLearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.greatLearning.bean.Users;
@Repository
public interface UserDao extends JpaRepository<Users,Integer>{
}
