package com.greatLearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.greatLearning.bean.Login;

@Repository
public interface LoginDao extends JpaRepository<Login,Integer>{

}
