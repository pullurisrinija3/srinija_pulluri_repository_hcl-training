package com.greatLearning.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="likedbooks")
public class Like {
	@Id
	private int id;
	@Column(name="bookname")
	private String BookName;
	@Column(name="booktype")
	private String BookType;
	@Column(name="bookauthor")
	private String BookAuthor;
	@Column(name="bookimage")
	private String BookImage;
	@Column(name="bookprice")
	private float BookPrice;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBookName() {
		return BookName;
	}
	public void setBookName(String bookName) {
		this.BookName = bookName;
	}
	public String getBookType() {
		return BookType;
	}
	public void setBookType(String bookType) {
		this.BookType = bookType;
	}
	public String getBookAuthor() {
		return BookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		this.BookAuthor = bookAuthor;
	}
	public String getBookImage() {
		return BookImage;
	}
	public void setBookImage(String bookImage) {
		this.BookImage = bookImage;
	}
	public float getBookPrice() {
		return BookPrice;
	}
	public void setBookPrice(float bookPrice) {
		this.BookPrice = bookPrice;
	}
	@Override
	public String toString() {
		return "Book [id=" + id + ", BookName=" + BookName + ", BookType=" + BookType + ", BookAuthor="
				+ BookAuthor + ", BookImage=" + BookImage + ", BookPrice=" + BookPrice + "]";
	}
}
