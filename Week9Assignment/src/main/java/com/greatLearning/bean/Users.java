package com.greatLearning.bean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Users {
	@Id
	@Column(name="userid")
	private int userId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	private String username;
	@Column(name="email")
	private String Email;
	@Column(name="password")
	private String password;
	@Column(name="retypepassword")
	private String retypePassword;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		this.Email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRetypePassword() {
		return retypePassword;
	}
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	@Override
	public String toString() {
		return "Users [userId=" + userId +",username=" + username + ", Email=" + Email + ", password=" + password + ", retypePassword="
				+ retypePassword + "]";
	}
	
}
