package com.greatLearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@SpringBootApplication(scanBasePackages="com.greatLearning")
@EntityScan(basePackages = "com.greatLearning.bean")				
@EnableJpaRepositories(basePackages = "com.greatLearning.dao")			
public class Week9AssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9AssignmentApplication.class, args);
		System.err.println("Server running on port number 9090");
	}

}
