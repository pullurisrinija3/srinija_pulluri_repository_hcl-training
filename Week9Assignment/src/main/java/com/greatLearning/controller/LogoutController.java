package com.greatLearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.greatLearning.service.LogoutService;

@RestController
@RequestMapping("/Login")
public class LogoutController {
	@Autowired
	LogoutService logoutService;
	@DeleteMapping(value = "Logout/{loginid}")
	public String storeProductInfo(@PathVariable("loginid") int loginid)
	{
	return logoutService.deleteLoginDetails(loginid);
	}

}
