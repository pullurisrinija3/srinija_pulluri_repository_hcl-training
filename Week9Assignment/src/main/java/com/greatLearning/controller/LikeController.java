package com.greatLearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.greatLearning.bean.Like;
import com.greatLearning.service.LikeService;

@RestController
@RequestMapping("/Like")
public class LikeController {
	@Autowired
	LikeService likeService;
	@PostMapping(value = "storeLikedBookDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProductInfo(@RequestBody Like like) 
	{	
	return likeService.storeLikedBookDetails(like);
	}
	@GetMapping(value = "getAllLikedBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Like> getAllBookDetails()
	{
	return likeService.getAllLikedBooks();
	}
}
