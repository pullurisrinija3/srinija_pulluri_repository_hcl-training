create database travel_hcl_SrinijaPulluri;

use travel_hcl_SrinijaPulluri;

create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);


create table PRICE(Bus_Type varchar(20),Distance int, Price int);

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;


Select Gender, count(Gender) as 'count of passengers travelled for minimum distance of 600km' from passenger p where distance >= 600 group by gender;

SELECT Min(Price) as 'minimum ticket price',bus_type FROM PRICE where bus_type='sleeper';

select * from PASSENGER where passenger_name like 'S%';

SELECT DISTINCT p.passenger_name,p.boarding_city,p.destination_city,p.bus_type,
pr.price as 'calculated price' FROM passenger p JOIN price pr ON p.bus_type = pr.bus_type
AND p.distance = pr.distance ORDER BY p.passenger_name;

Select p.passenger_name, pr.price as 'ticket price'  from passenger p join price pr on p.bus_type ='sitting'
and p.distance >= 1000 group by p.passenger_name;

SELECT p.passenger_name,p.destination_city AS boarding_city,p.boarding_city AS destination_city, pr.bus_type,pr.price as bus_charge FROM passenger p JOIN  price pr ON pr.distance = p.distance
WHERE p.passenger_name = 'Pallavi'; 

Select distinct (p.distance) as 'unique distance' from passenger p order by p.distance desc;

Select passenger_name,distance as 'distance travelled',distance*100/ (Select sum(distance) from passenger)as 'Percentage of Distance travelled' from passenger;

CREATE VIEW Query AS SELECT * FROM PASSENGER WHERE Category = 'AC';
select * from Query;

delimiter //
create procedure sleeper_list() 
begin
select passenger_name,Bus_Type from passenger  where Bus_Type='Sleeper';
end//
call sleeper_list();

select * from passenger limit 5;
